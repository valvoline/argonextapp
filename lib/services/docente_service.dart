import 'dart:convert';
import 'package:argo_next/models/docente.dart';
import 'package:http/http.dart';

class DocenteService {
  static Future<List<Docente>> getItems(String baseURL) {
    return get(baseURL).then((onResponse) {
      var decoded = jsonDecode(onResponse.body);
      if (decoded is List<dynamic>) {
        List<Docente> retArray = [];
        for (final a in decoded) {
          retArray.add(Docente.fromJson(a));
        }
        return retArray;
      }
      throw Exception('json parsing failure');
    }).catchError((onError) {
      print('[ERRROR] - getItems: $onError');
      return null;
    });
  }
}

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:argo_next/models/utente.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum LoggedInState { unknown, loggedin, notloggedin }

class UtenteService extends ChangeNotifier {
  Utente _internalUser;

  LoggedInState isLoggedIn() {
    if (_internalUser == null) {
      return LoggedInState.unknown;
    }
    return (_internalUser.token != null)
        ? LoggedInState.loggedin
        : LoggedInState.notloggedin;
  }

  void _storeUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tmpString = jsonEncode(_internalUser.toJson());
    await prefs.setString('storedUser', tmpString);
  }

  void _restoreUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var storedString = prefs.getString('storedUser');
    if (storedString != null) {
      _internalUser = Utente.fromJson(json.decode(storedString));
    } else {
      _internalUser = Utente();
    }
    notifyListeners();
  }

  void _removeStoredUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('storedUser');
  }

  final String _baseURL =
      'https://my-json-server.typicode.com/valvoline/demo/login';

  UtenteService() {
    _restoreUser();
  }

  static Future<List<Utente>> getItems(String baseURL) {
    return get(baseURL).then((onResponse) {
      var decoded = jsonDecode(onResponse.body);
      if (decoded is List<dynamic>) {
        List<Utente> retArray = [];
        for (final a in decoded) {
          retArray.add(Utente.fromJson(a));
        }
        return retArray;
      }
      throw Exception('json parsing failure');
    }).catchError((onError) {
      print('[ERRROR] - getItems: $onError');
      return null;
    });
  }

  Future<dynamic> login(String username, String password) {
    return get(_baseURL).then((onResponse) {
      var decoded = jsonDecode(onResponse.body);
      if (decoded is List<dynamic>) {
        for (final a in decoded) {
          Utente anUser = Utente.fromJson(a);
          if (anUser.username == username && anUser.password == password) {
            _internalUser = anUser;
            _storeUser();
            notifyListeners();
            return null;
          }
        }
      }
      throw Exception('user not found');
    }).catchError((onError) {
      print('[ERRROR] - getItems: $onError');
      _internalUser = Utente();
      _removeStoredUser();
      notifyListeners();
      return onError;
    });
  }

  void logout() {
    _internalUser = Utente();
    _removeStoredUser();
    notifyListeners();
  }

  void refreshToken() {
    print('refreshing token...');
    get(_baseURL).then((onResponse) {
      var decoded = jsonDecode(onResponse.body);
      if (decoded is List<dynamic>) {
        for (final a in decoded) {
          Utente anUser = Utente.fromJson(a);
          if (anUser.username == _internalUser.username &&
              anUser.password == _internalUser.password) {
            if (anUser.token == _internalUser.token) {
              //è valido

            } else {
              //non è valido
              logout();
            }
            break;
          }
        }
      }
    });
  }
}

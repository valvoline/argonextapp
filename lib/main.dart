import 'package:flutter/material.dart';
import 'package:argo_next/services/utente_service.dart';
import 'package:argo_next/shared/captive_wrapper.dart';
import 'package:provider/provider.dart';

void main() => runApp(
      ChangeNotifierProvider(
        create: (context) => UtenteService(),
        child: Consumer<UtenteService>(
          builder: (context, data, child) {
            return CaptiveWrapper(
              isLoggedIn: data.isLoggedIn(),
            );
          },
        ),
      ),
    );

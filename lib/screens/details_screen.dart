import 'package:flutter/material.dart';
import 'package:argo_next/models/docente.dart';
import 'package:argo_next/shared/centered_avatar.dart';
import 'package:argo_next/shared/dettagli_contatto.dart';
import 'package:argo_next/shared/riga_contatto.dart';

class DetailsScreen extends StatefulWidget {
  final Docente argument;

  DetailsScreen({Key key, this.argument});

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  Docente data;

  @override
  Widget build(BuildContext context) {
    data = widget.argument;

    if (data == null) {
      data = Docente(nome: "", email: "", scuola: "", thumb: "", telefono: "");
    }

    return Scaffold(
      backgroundColor: Colors.brown[50],
      appBar: AppBar(
        primary: false,
        centerTitle: true,
        title: Text('Dettaglio docente'),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CenteredAvatar(
                  url: data.thumb,
                  radius: 48.0,
                ),
                Divider(
                  height: 60.0,
                  color: Colors.grey[500],
                ),
                DettagliContatto(nome: data.nome, scuola: data.scuola),
                Divider(
                  height: 60.0,
                  color: Colors.grey[500],
                ),
                RigaContatto(
                  icon: Icons.email,
                  rightText: data.email,
                ),
                SizedBox(
                  height: 16.0,
                ),
                RigaContatto(
                  icon: Icons.phone,
                  rightText: data.telefono,
                ),
              ]),
        ),
      ),
    );
  }
}

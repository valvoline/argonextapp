import 'package:flutter/material.dart';
import 'package:argo_next/screens/home_screen.dart';
import 'package:argo_next/services/utente_service.dart';
import 'package:provider/provider.dart';

class DrawerScreen extends StatefulWidget {
  DrawerScreen({Key key});

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false, // Used for removing back buttoon.
        title: Text('Impostazioni'),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Card(
              child: ListTile(
                onTap: () {
                  Navigator.pop(context);
                  homeNavigatorKey.currentState.pushReplacementNamed('/news');
                },
                title: Text('News'),
              ),
            ),
            Card(
              child: ListTile(
                onTap: () {
                  Navigator.pop(context);
                  homeNavigatorKey.currentState
                      .pushReplacementNamed('/contatti');
                },
                title: Text('Contatti'),
              ),
            ),
            Card(
              child: ListTile(
                onTap: () {
                  Provider.of<UtenteService>(context, listen: false).logout();
                },
                title: Text('Logout'),
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:argo_next/models/docente.dart';
import 'package:argo_next/screens/home_screen.dart';
import 'package:argo_next/services/docente_service.dart';

class ContactsList extends StatefulWidget {
  @override
  _ContactsListState createState() => _ContactsListState();
}

class _ContactsListState extends State<ContactsList> {
  bool _isLoading = false;
  List<Docente> _items = [];

  void _reloadData() async {
    setState(() => _isLoading = true);
    var response = await DocenteService.getItems(
        'https://my-json-server.typicode.com/valvoline/demo/contatti');
    setState(() => _isLoading = false);
    if (response != null) {
      setState(() {
        _items = response;
      });
    } else {
      print('Errore nel caricamento');
    }
  }

  @override
  void initState() {
    super.initState();
    _reloadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: true,
      backgroundColor: Colors.brown[50],
      body: Container(
        child: (_isLoading)
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: _items.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      onTap: () {
                        print("index: $index");
                        Docente tmpDocente = _items[index];
                        homeNavigatorKey.currentState
                            .pushNamed('/dettaglio', arguments: tmpDocente);
                      },
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage(_items[index].thumb),
                      ),
                      title: Text(_items[index].nome),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  );
                },
              ),
      ),
    );
  }
}

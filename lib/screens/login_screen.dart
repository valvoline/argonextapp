import 'package:flutter/material.dart';
import 'package:argo_next/services/utente_service.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var _formKey = GlobalKey<FormState>();
  String _tmpUsername = "";
  String _tmpPassword = "";
  bool _buttonEnabled = false;
  FocusNode myFocusNode;

  void _login(UtenteService dataPtr) async {
    if (_formKey.currentState.validate() == true) {
      print('form valido.');
      dataPtr.login(_tmpUsername, _tmpPassword).catchError((onError) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: new Text("Alert Dialog title"),
              content: new Text("Alert Dialog body"),
            );
          },
        );
      });
    } else {
      print('form non valido');
    }
  }

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[50],
      appBar: AppBar(
        centerTitle: true,
        title: SizedBox(
          height: 60,
          child: Image.asset('assets/logo.png'),
        ),
      ),
      body: Consumer<UtenteService>(
        builder: (context, data, child) {
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(16),
              child: Center(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (v) {
                          myFocusNode.requestFocus();
                        },
                        onChanged: (val) {
                          _tmpUsername = val;
                          setState(() {
                            _buttonEnabled = (_tmpUsername.length > 0 &&
                                _tmpPassword.length > 0);
                          });
                        },
                        decoration: InputDecoration(
                          icon: Icon(Icons.perm_identity),
                          hintText: "nome utente",
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey[800],
                                style: BorderStyle.solid,
                                width: 1.0),
                          ),
                        ),
                        validator: (val) {
                          return (val.length > 4)
                              ? null
                              : "Nome utente non valido.";
                        },
                      ),
                      SizedBox(height: 16.0),
                      TextFormField(
                        focusNode: myFocusNode,
                        textInputAction: TextInputAction.done,
                        onFieldSubmitted: (v) {
                          _login(data);
                        },
                        onChanged: (val) {
                          _tmpPassword = val;
                          setState(() {
                            _buttonEnabled = (_tmpUsername.length > 0 &&
                                _tmpPassword.length > 0);
                          });
                        },
                        obscureText: true,
                        decoration: InputDecoration(
                          icon: Icon(Icons.vpn_key),
                          hintText: "password",
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Colors.grey[800],
                                style: BorderStyle.solid,
                                width: 1.0),
                          ),
                        ),
                        validator: (val) {
                          return (val.length > 4)
                              ? null
                              : "Password non valida.";
                        },
                      ),
                      SizedBox(
                        height: 24.0,
                      ),
                      RaisedButton(
                        padding: EdgeInsets.fromLTRB(50, 16, 50, 16),
                        onPressed: (_buttonEnabled) ? () => _login(data) : null,
                        color: Colors.greenAccent[700],
                        child: Text(
                          'Login',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

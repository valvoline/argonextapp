import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:argo_next/screens/details_screen.dart';
import 'package:argo_next/screens/drawer_screen.dart';
import 'package:argo_next/screens/listcontacts_screen.dart';
import 'package:argo_next/screens/news_screen.dart';

final GlobalKey<NavigatorState> homeNavigatorKey =
    new GlobalKey<NavigatorState>();

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  void setupPushNotifications() {
    if (Platform.isIOS) {
      _firebaseMessaging.requestNotificationPermissions(
          IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });
    }

    _firebaseMessaging.getToken().then((token) {
      print("firebaseToken: $token");
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
      onBackgroundMessage: null,
    );
  }

  @override
  void initState() {
    super.initState();
    setupPushNotifications();
  }

  @override
  Widget build(BuildContext context) {
    var tmpWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.brown[50],
      appBar: AppBar(
        title: Text('ArgoNext'),
      ),
      body: Container(
        child: Navigator(
            key: homeNavigatorKey,
            initialRoute: '/news',
            onGenerateRoute: (routeSettings) {
              Widget retWidget = HomePage();
              switch (routeSettings.name) {
                case '/contatti':
                  retWidget = ContactsList();
                  break;
                case '/dettaglio':
                  retWidget = DetailsScreen(
                    argument: routeSettings.arguments,
                  );
                  break;
                case '/news':
                  retWidget = NewsScreen();
                  break;
                default:
                  break;
              }
              return MaterialPageRoute(builder: (context) => retWidget);
            }),
      ),
      drawer: SizedBox(
        width: tmpWidth - 100,
        child: DrawerScreen(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

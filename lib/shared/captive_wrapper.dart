import 'package:flutter/material.dart';
import 'package:argo_next/screens/home_screen.dart';
import 'package:argo_next/screens/login_screen.dart';
import 'package:argo_next/screens/splash_screen.dart';
import 'package:argo_next/services/utente_service.dart';
import 'package:provider/provider.dart';

class CaptiveWrapper extends StatefulWidget {
  final LoggedInState isLoggedIn;

  CaptiveWrapper({Key key, this.isLoggedIn});

  @override
  _CaptiveWrapperState createState() => _CaptiveWrapperState();
}

class _CaptiveWrapperState extends State<CaptiveWrapper>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Provider.of<UtenteService>(context, listen: false).refreshToken();
    }
  }

  @override
  Widget build(BuildContext context) {
    Map<String, Widget Function(BuildContext)> tmpRoutes;
    switch (widget.isLoggedIn) {
      case LoggedInState.loggedin:
        tmpRoutes = {'/': (context) => HomeScreen()};
        break;
      case LoggedInState.notloggedin:
        tmpRoutes = {'/': (context) => LoginScreen()};
        break;
      case LoggedInState.unknown:
        tmpRoutes = {'/': (context) => SplashScreen()};
        break;
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: GlobalKey(),
      routes: tmpRoutes,
    );
  }
}

import 'package:flutter/material.dart';

class CenteredAvatar extends StatelessWidget {
  final String url;
  final double radius;
  const CenteredAvatar({
    Key key,
    this.url,
    this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircleAvatar(
        backgroundImage: NetworkImage(url),
        radius: radius,
      ),
    );
  }
}

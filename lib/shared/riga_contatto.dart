import 'package:flutter/material.dart';

class RigaContatto extends StatelessWidget {
  final IconData icon;
  final String rightText;

  const RigaContatto({Key key, this.icon, this.rightText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      //Email
      children: <Widget>[
        Icon(
          icon,
          size: 24,
          color: Colors.grey[400],
        ),
        SizedBox(
          width: 8.0,
        ),
        Text(
          rightText,
          style: TextStyle(
            color: Colors.grey[400],
            fontSize: 18.0,
          ),
        ),
      ],
    );
  }
}

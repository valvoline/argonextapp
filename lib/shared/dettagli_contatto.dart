import 'package:flutter/material.dart';

class DettagliContatto extends StatelessWidget {
  final String nome;
  final String scuola;

  const DettagliContatto({
    Key key,
    this.nome,
    this.scuola,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          //Nome
          'NOME',
          style: TextStyle(
            color: Colors.grey,
            letterSpacing: 2.0,
          ),
        ),
        SizedBox(
          height: 8.0,
        ),
        Text(
          nome,
          style: TextStyle(
            color: Colors.grey[800],
            letterSpacing: 1.0,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(
          height: 24.0,
        ),
        Text(
          'SCUOLA DI APPARTENENZA',
          style: TextStyle(
            color: Colors.grey,
            letterSpacing: 2.0,
          ),
        ),
        SizedBox(
          height: 8.0,
        ),
        Text(
          scuola,
          style: TextStyle(
            color: Colors.grey[800],
            letterSpacing: 1.0,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}

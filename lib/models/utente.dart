class Utente {
  String username;
  String password;
  String token;
  int level;

  bool isTokenValid() {
    return (token != null);
  }

  Utente({this.username, this.password, this.token, this.level});

  Utente.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        password = json['password'],
        token = json['token'],
        level = json['level'];

  Map<String, dynamic> toJson() => {
        'username': username,
        'password': password,
        'token': token,
        'level': level
      };
}

class Docente {
  String nome;
  String scuola;
  String email;
  String telefono;
  String thumb;

  Docente({this.nome, this.scuola, this.email, this.telefono, this.thumb});

  Docente.fromJson(Map<String, dynamic> json)
      : nome = json['nome'],
        scuola = json['scuola'],
        email = json['email'],
        telefono = json['telefono'],
        thumb = json['thumb'];
}

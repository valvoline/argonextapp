# ArgoNext App

## A complete app including the following topics:

- Stateless and Statefull Widgets
- Navigation, Navigator and MaterialApp
- Push notification handling
- SharedPreferences and Persistence
- ValueNotifier
- Modal and Page Routing
- Provider and Data injection
- State management
- Network and Services
- JSON serialization and deserialization
- HTTP requests and handling
- Future, Async and Await
- Conditional loading and Ternary Operator


## Resources:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

- [Flutter online documentation](https://flutter.dev/docs)

- [Navigate without context in Flutter with a Navigation Service](https://medium.com/flutter-community/navigate-without-context-in-flutter-with-a-navigation-service-e6d76e880c1c)

- [Using FutureBuilder to Create a Better Widget](https://medium.com/@jimmyhott/using-futurebuilder-to-create-a-better-widget-4c7d4f52a329)

- [Flutter: Advance Routing and Navigator Part 1](https://dev.to/nitishk72/flutter-advance-routing-and-navigator-part-1-547l)

- [Dart asynchronous programming: Futures](https://medium.com/dartlang/dart-asynchronous-programming-futures-96937f831137)

- [From Flutter to Flight 3: Navigation](https://medium.com/@larsenthomasj/from-flutter-to-flight-3-navigation-8d567d2cb011)

- [Parsing complex JSON in Flutter](https://medium.com/flutter-community/parsing-complex-json-in-flutter-747c46655f51)

- [Flutter Alert Dialogs](https://medium.com/@nils.backe/flutter-alert-dialogs-9b0bb9b01d28)
